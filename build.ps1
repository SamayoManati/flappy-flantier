# build.ps1
#
# This script compiles Flappy Flantier
#
# To allow powershell to run scripts, open a powershell console, and write this :
# Set-ExecutionPolicy -ExecutionPolicy Unrestricted -Scope CurrentUser

param(
	[switch]$help = $false
)

if ($help) {
	write-output "===================="
	write-output "build.ps1"
	write-output "===================="
	write-output ""
	write-output "This script compiles Flappy Flantier into a .love file"
	write-output ""
}
else {
	write-output "===================="
	write-output "Building Flappy Flantier"
	write-output "===================="
	write-output ""
	remove-item ./ff.love
	compress-archive -path ./** -destinationPath ./ff.zip
	rename-item "ff.zip" "ff.love"
	write-output "===================="
	write-output "Building finished"
	write-output "===================="
	write-output ""
	pause
}