--namespace "FlappyFlantier.Entities"

require "Data/GlobalVariables"
require "Data/GlobalFunctions"

class "PipePair"
	property "x" {Type = "System.Number"}
	property "y" {Type = "System.Number"}
	property "remove" {Type = "System.Boolean"}
	property "pipes"
	property "gapHeight" {Type = "System.Number"}

	function PipePair(self, y)
		eax = love.graphics.newImage(PIPE_SPRITE)

		self.x = VIRTUAL_WIDTH + 32
		self.y = y
		self.gapHeight = GAP_HEIGHT
		-- on crée une nouvelle valeur random pour le GAP_HEIGHT
		-- commenter la ligne suivante pour désactiver la taille aléatoire.
		self.gapHeight = math.random(GAP_HEIGHT_MIN, GAP_HEIGHT_MAX)
		self.pipes = {
			['upper'] = Pipe('top', self.y),
			['lower'] = Pipe('bottom', self.y + eax:getHeight() + self.gapHeight)
		}
		self.remove = false
	end

	function update(self, deltaTime)
		-- supprime le PipePair s'il n'est plus à l'écran
		-- sinon, le bouge
		if self.x > -self.pipes['lower']:getHeight() then
			self.x = self.x - PIPE_SCROLL_SPEED * deltaTime
			self.pipes['lower'].x = self.x
			self.pipes['upper'].x = self.x
		else
			self.remove = true
		end
	end

	function draw(self)
		for k, v in pairs(self.pipes) do
			v:draw()
		end
	end
endclass "PipePair"