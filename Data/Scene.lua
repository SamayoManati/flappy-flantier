--namespace "FlappyFlantier.Entities"

import "System"

enum "SceneType" {
	"Level",
	"Menu",
}

interface "Scene"
	__Require__()
	property "type" {Type = SceneLevel}
	
	__Require__()
	function draw() end
	
	__Require__()
	function update(deltaTime) end
endinterface "Scene"
