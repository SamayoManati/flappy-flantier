--namespace "FlappyFlantier.Core"

require "Data/GlobalVariables"

function wasKeyPressed(key)
	return KEYSPRESSED[key]
end