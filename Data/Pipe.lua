--namespace "FlappyFlantier.Entities"

require "Data/GlobalVariables"
require "Data/GlobalFunctions"

local PIPE_IMAGE = love.graphics.newImage(PIPE_SPRITE)

class "Pipe"
	property "xpos" {Type = "System.Number"}
	property "ypos" {Type = "System.Number"}
	property "width" {Type = "System.Number"}
	property "height" {Type = "System.Number"}
	property "orientation" {Type = "System.String"}

	function getWidth(self)
		return self.width
	end

	function getHeight(self)
		return self.height
	end

	function getXPos(self)
		return self.xpos
	end

	function getYPos(self)
		return self.ypos
	end

	function setXPos(self, xpos)
		self.xpos = xpos
	end

	function setYPos(self, ypos)
		self.ypos = ypos
	end

	function Pipe(self, orientation, y)
		self.xpos = VIRTUAL_WIDTH
		self.ypos = y
		self.width = PIPE_IMAGE:getWidth()
		self.height = PIPE_IMAGE:getHeight()
		self.orientation = orientation
	end

	function update(self, deltaTime)
		-- vide
		-- car la logique d'updating est dans la classe PipePair
	end

	function draw(self)
		love.graphics.draw(PIPE_IMAGE, self.xpos, (self.orientation == 'top' and self.ypos + self.height or self.ypos), 0, 1, self.orientation == 'top' and -1 or 1)
	end
endclass "Pipe"
