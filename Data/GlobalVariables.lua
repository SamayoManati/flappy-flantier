--namespace "FlappyFlantier.Core"

VERSION_NAME = "Flappy Flantier - version alpha1 @ build 6 (09-06-2018)"
TIP = [[Press F12 to fullscreen
Press space to jump]]

--[graphics]
BACKGROUND_SCROLL_SPEED = 30
GROUND_SCROLL_SPEED = 60
BACKGROUND_LOOPING_POINT = 413
PIPE_SCROLL_SPEED = -GROUND_SCROLL_SPEED
PLAYER_SPRITE = "Graphics/Characters/Flantier.png"
PIPE_SPRITE = "Graphics/Characters/Pipe.png"
GAP_HEIGHT = 90 -- la taille du trou entre les deux obstacles (haut et bas) d'un PipePair
				-- plus la valeur est petite, plus les deux pipes seront resserés donc plus le jeu
				-- sera difficile
GAP_HEIGHT_MAX = GAP_HEIGHT + 20
GAP_HEIGHT_MIN = GAP_HEIGHT - 20

--[physics]
GRAVITY = 10 -- the less the gravity is, the slower the player will fall

--[variables push]
WINDOW_WIDTH = 1280
WINDOW_HEIGHT = 720
VIRTUAL_WIDTH = 512
VIRTUAL_HEIGHT = 288

--[keyboard]
KEYSPRESSED = {}