--namespace "FlappyFlantier.Entities"

require "Data/GlobalVariables"
require "Data/GlobalFunctions"

class "Player"
	property "xpos" {Type = "System.Number"}
	property "ypos" {Type = "System.Number"}
	property "width" {Type = "System.Number"}
	property "height" {Type = "System.Number"}
	property "isJumping" {Type = "System.Boolean"}
	property "jump" {Type = "System.Number"}
	property "image"
	property "deltaY" {Type = "System.Number"}
	
	--function Player(self, VIRTUAL_WIDTH, VIRTUAL_HEIGHT, WINDOW_WIDTH, WINDOW_HEIGHT)
	function Player(self)
		self.xpos = VIRTUAL_WIDTH / 2
		self.ypos = VIRTUAL_HEIGHT
		self.isJumping = false
		self.jump = 10
		self.image = love.graphics.newImage(PLAYER_SPRITE)
		self.width = self.image:getWidth()
		self.height = self.image:getHeight()
		self.deltaY = 0
		
		self.xpos = (VIRTUAL_WIDTH / 2) - (self.width / 2)
		self.ypos = (VIRTUAL_HEIGHT / 2) - (self.height / 2)
	end
	
	function getXPos(self)
		return self.xpos
	end
	
	function getYPos(self)
		return self.ypos
	end
	
	function setXPos(self, xpos)
		self.xpos = xpos
	end
	
	function setYPos(self, ypos)
		self.ypos = ypos
	end
	
	function getWidth(self)
		return self.width
	end
	
	function getHeight(self)
		return self.height
	end
	
	function getIsJumping(self)
		return self.isJumping
	end
	
	function setIsJumping(self, b)
		self.isJumping = b
	end
	
	function getJump(self)
		return self.jump
	end
	
	function setJump(self, jmp)
		self.jump = jmp
	end
	
	function setImage(self, filepath)
		self.image = love.graphics.newImage(filepath)
		self.height = self.image:getheight()
		self.width = self.image:getwidth()
	end
	
	function getImage(self)
		return self.image
	end

	function getDeltaY(self)
		return self.deltaY
	end

	function setDeltaY(self, deltaY)
		self.deltaY = deltaY
	end
	
	function update(self, deltaTime, ___testarg_height___)
		-- gravity
		self:setDeltaY(self:getDeltaY() + GRAVITY * deltaTime)
		self:setYPos(self:getYPos() + self:getDeltaY())

		if wasKeyPressed("space") then
			self:setDeltaY(-5)
		end
		
		--if self:getYPos() + self:getHeight() >= VIRTUAL_HEIGHT then
		if self:getYPos() + self:getHeight() >= VIRTUAL_HEIGHT - ___testarg_height___ then
			--self:setYPos(VIRTUAL_HEIGHT - self:getHeight())
			self:setYPos(VIRTUAL_HEIGHT - self:getHeight() - ___testarg_height___)
		end
		if self:getYPos() < 0 then
			self:setYPos(0)
		end
		if self:getXPos() + self:getWidth() >= VIRTUAL_WIDTH then
			self:setXPos(VIRTUAL_WIDTH - self:getWidth())
		end
		if self:getXPos() < 0 then
			self:setXPos(0)
		end
	end
	
	function draw(self, ___testarg_height___)
		love.graphics.draw(self:getImage(), self:getXPos(), self:getYPos())
	end
	
	function keyreleased(self, key)
		if key == "space" then
			player:setYPos(player:getYPos() + player:getJump())
		end
	end
endclass "Player"
