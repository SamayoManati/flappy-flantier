--namespace "FlappyFlantier.Entities"

import "System"

__Abstract__()
class "ScenesManager"
	--[[
	la classe qui gère les scènes du jeu.
	]]--
	
	property "actualScene" {Type = Scene}
endclass "ScenesManager"