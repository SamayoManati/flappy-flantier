--[[
FLAPPY FLANTIER

DEBUG INFOS :
bs = background scroll
gs = ground scroll
y = player Y
x = player
dy = player delta Y
dt = delta time




pour ajouter de la doc :
_ _ D o c _ _   [ [ d o c ] ]
]]

--namespace FlappyFlantier.Core

function love.load()
	require "PLoop"
	tick = require "System/tick"
	require "System/color"
	push = require "System/push"
	require "Data/Player"
	require "Data/Scene"
	require "Data/ScenesManager"
	require "Data/GlobalVariables"
	require "Data/Pipe"
	require "Data/PipePair"

	math.randomseed(os.time())
	
	-- pas d'interpolation de pixels lors du redimentionnement des images
	love.graphics.setDefaultFilter("nearest", "nearest")
	
	-- test scene
	background = love.graphics.newImage("Graphics/Parallax/Test.png")
	ground = love.graphics.newImage("Graphics/Parallax/TestGround.png")
	backgroundScroll = 0
	groundScroll = 0
	pipes = {}
	pipesSpawnTimer = 0
	player = Player()
	eax = love.graphics.newImage(PIPE_SPRITE)
	lastY = -(eax:getHeight()) + math.random(80) + 20
	
	push:setupScreen(VIRTUAL_WIDTH, VIRTUAL_HEIGHT, WINDOW_WIDTH, WINDOW_HEIGHT, {
		vsync      = true,
		fullscreen = false,
		resizable  = true
	})

	trackdt = 0 -- remove the variable in the final version
end


function love.resize(w, h)
	push:resize(w, h)
end


function love.update(deltaTime)
	-- test scene
	backgroundScroll = (backgroundScroll + BACKGROUND_SCROLL_SPEED * deltaTime) % BACKGROUND_LOOPING_POINT
	groundScroll = (groundScroll + GROUND_SCROLL_SPEED * deltaTime) % VIRTUAL_WIDTH
	pipesSpawnTimer = pipesSpawnTimer + deltaTime
	if pipesSpawnTimer > 2 then
		local y = math.max(-(eax:getHeight()) + 10, math.min(lastY + math.random(-20, 20), VIRTUAL_HEIGHT - 90 - eax:getHeight()))
		lastY = y

		table.insert(pipes, PipePair(y))
		pipesSpawnTimer = 0
	end
	player:update(deltaTime, ground:getHeight())
	for k, v in pairs(pipes) do
		v:update(deltaTime)
		
		if v.remove then
			table.remove(pipes, k)
		end
	end

	trackdt = deltaTime


	KEYSPRESSED = {}
end


function love.draw()
	push:start()
	-- test scene
	love.graphics.draw(background, -backgroundScroll, 0)
	love.graphics.draw(ground, -groundScroll, VIRTUAL_HEIGHT - ground:getHeight())
	player:draw(ground:getHeight())
	for k, v in pairs(pipes) do
		v:draw()
	end
	
	love.graphics.print({colorsvg.Red, VERSION_NAME .. "\n" .. TIP}, 0, 0)
	love.graphics.print({colorsvg.Red, "bs:" .. backgroundScroll .. "\n" ..
		"gs:" .. groundScroll .. "\n" ..
		" y:" .. player:getYPos() .. "\n" ..
		" x:" .. player:getXPos() .. "\n" ..
		"dy:" .. player:getDeltaY() .. "\n" ..
		"dt:" .. trackdt}, VIRTUAL_WIDTH - 140, 0)
	push:finish()
end


function love.keypressed(key)
	KEYSPRESSED[key] = true
end


function love.keyreleased(key)
	if key == "f12" then
		love.window.setFullscreen(not love.window.getFullscreen())
	end
	
	player:keyreleased(key)
end
