--[[
CONFIGURATION FILE FOR THE APPLICATION
]]

require "Data/GlobalVariables"

function love.conf(game)
	game.console = false -- doit-on attacher la console?
	
	game.window.resizable = true -- peut-on changer la taille de la fen�tre de jeu?
	game.window.title = VERSION_NAME -- le nom de la fen�tre principale
	game.window.icon = "Graphics/System/game.png" -- l'icon de la fen�tre
	game.window.width = 800 -- la largeur de la fen�tre
	game.window.height = 600 -- la hauteur de la fen�tre
	game.window.borderless = false -- si true, retire les bordures de la fen�tre (donc le titre, l'icon et les boutons)
	game.window.fullscreen = false -- met le jeu en mode plein �cran
	game.window.vsync = true -- autorise ou non la synchronisation verticale
	
	game.modules.audio = true -- importe ou non le module audio
	game.modules.event = true -- importe ou non le module event
	game.modules.graphics = true -- importe ou non le module graphics. A BESOIN DU MODULE `window` POUR FONCTIONNER.
	game.modules.image = true -- importe ou non le module image
	game.modules.joystick = true -- importe ou non le module joystick
	game.modules.keyboard = true -- importe ou non le module keyboard
	game.modules.math = true -- importe ou non le module math
	game.modules.mouse = true -- importe ou non le module mouse
	game.modules.physics = true -- importe ou non le module physics
	game.modules.sound = true -- importe ou non le module sound
	game.modules.system = true -- importe ou non le module system
	game.modules.timer = true -- importe ou non le module timer. DESACTIVER LE MODULE TIMER PRODUIRA UN DELTATIME DE 0 DANS `love.update`
	game.modules.touch = true -- importe ou non le module touch
	game.modules.video = true -- importe ou non le module video
	game.modules.window = true -- importe ou non le module window
	game.modules.thread = true -- importe ou non le module thread
	
	game.modules.font = true -- importe ou non le module font. DESACTIVER LE MODULE FONT EMP�CHERA LE LANCEMENT DU JEU!
end
