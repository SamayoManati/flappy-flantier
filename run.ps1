# run.ps1
#
# This script runs Flappy Flantier

param(
	[switch]$help = $false
)

if ($help) {
	write-output "=========="
	write-output "run.ps1"
	write-output "=========="
	write-output "Runs Flappy Flantier"
	write-output ""
}
else {
	if ((test-path "./ff.love") -eq $false) {
		./build.ps1
	}
	./../../builder/love.exe ./ff.love
}